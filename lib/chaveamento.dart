import 'package:bidirectional_scroll_view/bidirectional_scroll_view.dart';
import 'package:flutter/material.dart';

enum FASES {FASE1, FASE2, FASE3, FINAL}

class Chaveamento extends StatelessWidget {

  double alturaSizedBox;
  double radiusItemAvatar;
  double expessuraLinha;
  double comprimentoLinha;
  Color corLinha;
  Color corFundoAvatar;
  bool exibirLabel;
  bool exibirFase1;
  bool exibirFase2;
  bool exibirPontos;
  bool exibirQualificados;
  List<ItemAvatar> itensFase1;
  List<ItemAvatar> itensFase2;
  List<ItemAvatar> itensFase3;
  ItemAvatar itemFinal;


  Chaveamento({
    this.alturaSizedBox = 10,
    this.radiusItemAvatar = 40.0,
    this.expessuraLinha = 1.0,
    this.comprimentoLinha = 20.0,
    this.corLinha = Colors.black26,
    this.corFundoAvatar = Colors.black12,
    this.exibirLabel = false,
    this.exibirFase1 = true,
    this.exibirFase2 = true,
    this.exibirPontos = true,
    this.exibirQualificados = true,
    this.itensFase1,
    this.itensFase2,
    this.itensFase3,
    this.itemFinal
  });

  @override
  Widget build(BuildContext context) {

    List<Widget> widgets = List<Widget>();
    if (exibirFase1 || (exibirFase1 && !exibirFase2)) {
      widgets.add(_buildFase1(context));
      widgets.add(_buildFase2(context));
    } else if (!exibirFase1 && exibirFase2) {
      widgets.add(_buildFase2(context));
    }
    widgets.add(_buildFase3(context));
    widgets.add(_buildFinal(context));

    return BidirectionalScrollViewPlugin(
      child: Container(
        padding: EdgeInsets.all(10.0),
        color: Colors.white,
        child: Row(
          children: widgets,
        ),
      ),
      velocityFactor: 2.0,
      scrollOverflow: Overflow.clip,
    );
  }

  Widget _buildFinal(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: _calcularAlturaLinhaConfronto(FASES.FINAL),
          ),
          _buildItemAvatar(
            buildContext: context,
            itemAvatar: itemFinal,
            fase: FASES.FINAL
          ), 
        ],
      ),
    );
  }

  Widget _buildFase3(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: _calcularMargeFaseFase3(),
          ),
          _buildConfrontoFase3(
            context,
            item1: (itensFase3 != null && itensFase3.isNotEmpty) ? itensFase3[0] : ItemAvatar.getItemPadrao(),
            item2: (itensFase3 != null && itensFase3.isNotEmpty) ? itensFase3[1] : ItemAvatar.getItemPadrao(),
          ),
        ],
      ),
    );
  }

  Widget _buildConfrontoFase3(BuildContext context, {ItemAvatar item1, ItemAvatar item2}) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                _buildItemAvatar(
                  buildContext: context,
                  itemAvatar: item1,
                ), 
                SizedBox(
                  height: (radiusItemAvatar * 6) + (alturaSizedBox * 4),
                ),
                _buildItemAvatar(
                  buildContext: context,
                  itemAvatar: item2,
                ), 
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    color: corLinha,
                    width: comprimentoLinha,
                    height: expessuraLinha,
                  ),
                  SizedBox(
                    height: (radiusItemAvatar * 8) + (alturaSizedBox * 4),
                  ),
                  Container(
                    color: corLinha,
                    width: comprimentoLinha,
                    height: expessuraLinha,
                  ),
                ],
              ),
              Container(
                color: corLinha,
                width: expessuraLinha,
                height: _calcularAlturaLinhaConfronto(FASES.FASE3),
              ),
              Container(
                color: corLinha,
                width: comprimentoLinha,
                height: expessuraLinha,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildFase2(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: (radiusItemAvatar + (expessuraLinha * 2)),
          ),
          _buildConfrontoFase2(
            context,
            item1: (itensFase2 != null && itensFase2.isNotEmpty) ? itensFase2[0] : ItemAvatar.getItemPadrao(),
            item2: (itensFase2 != null && itensFase2.isNotEmpty) ? itensFase2[1] : ItemAvatar.getItemPadrao(),
          ),
          SizedBox(
            height: (radiusItemAvatar * 2) + (alturaSizedBox * 2),
          ),
          _buildConfrontoFase2(
            context,
            item1: (itensFase2 != null && itensFase2.isNotEmpty) ? itensFase2[2] : ItemAvatar.getItemPadrao(),
            item2: (itensFase2 != null && itensFase2.isNotEmpty) ? itensFase2[3] : ItemAvatar.getItemPadrao(),
          ),
        ],
      ),
    );
  }

  Widget _buildConfrontoFase2(BuildContext context, {ItemAvatar item1, ItemAvatar item2}) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                _buildItemAvatar(
                  buildContext: context,
                  itemAvatar: item1,
                ), 
                SizedBox(
                  height: (radiusItemAvatar * 2) + (alturaSizedBox * 2),
                ),
                _buildItemAvatar(
                  buildContext: context,
                  itemAvatar: item2,
                ), 
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Container(
                    color: corLinha,
                    width: comprimentoLinha,
                    height: expessuraLinha,
                  ),
                  SizedBox(
                    height: (radiusItemAvatar * 4) + (alturaSizedBox * 2),
                  ),
                  Container(
                    color: corLinha,
                    width: comprimentoLinha,
                    height: expessuraLinha,
                  ),
                ],
              ),
              Container(
                color: corLinha,
                width: expessuraLinha,
                height: _calcularAlturaLinhaConfronto(FASES.FASE2),
              ),
              Container(
                color: corLinha,
                width: comprimentoLinha,
                height: expessuraLinha,
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildFase1(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _buildConfrontoFase1(
            context,
            item1: (itensFase1 != null && itensFase1.isNotEmpty) ? itensFase1[0] : ItemAvatar.getItemPadrao(),
            item2: (itensFase1 != null && itensFase1.isNotEmpty) ? itensFase1[1] : ItemAvatar.getItemPadrao(),
          ),
          SizedBox(
            height: alturaSizedBox,
          ),
          _buildConfrontoFase1(
            context,
            item1: (itensFase1 != null && itensFase1.isNotEmpty) ? itensFase1[2] : ItemAvatar.getItemPadrao(),
            item2: (itensFase1 != null && itensFase1.isNotEmpty) ? itensFase1[3] : ItemAvatar.getItemPadrao(),
          ),
          SizedBox(
            height: alturaSizedBox,
          ),
          _buildConfrontoFase1(
            context,
            item1: (itensFase1 != null && itensFase1.isNotEmpty) ? itensFase1[4] : ItemAvatar.getItemPadrao(),
            item2: (itensFase1 != null && itensFase1.isNotEmpty) ? itensFase1[5] : ItemAvatar.getItemPadrao(),
          ),
          SizedBox(
            height: alturaSizedBox,
          ),
          _buildConfrontoFase1(
            context,
            item1: (itensFase1 != null && itensFase1.isNotEmpty) ? itensFase1[6] : ItemAvatar.getItemPadrao(),
            item2: (itensFase1 != null && itensFase1.isNotEmpty) ? itensFase1[7] : ItemAvatar.getItemPadrao(),
          ),
        ],
      ),
    );
  }

  Widget _buildConfrontoFase1(BuildContext context, {ItemAvatar item1, ItemAvatar item2}) {
    return Container(
      child: Row(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                _buildItemAvatar(
                  buildContext: context,
                  itemAvatar: item1,
                ),
                SizedBox(
                  height: alturaSizedBox,
                ),
                _buildItemAvatar(
                  buildContext: context,
                  itemAvatar: item2,
                ),          
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Column(
                children: <Widget>[                  
                  Container(
                    color: corLinha,
                    width: comprimentoLinha,
                    height: expessuraLinha,
                  ),
                  SizedBox(
                    height: (radiusItemAvatar * 2) + alturaSizedBox,
                  ),
                  Container(
                    color: corLinha,
                    width: comprimentoLinha,
                    height: expessuraLinha,
                  ),
                ],
              ),
              Container(
                color: corLinha,
                width: expessuraLinha,
                height: _calcularAlturaLinhaConfronto(FASES.FASE1),
              ),
              Container(
                color: corLinha,
                width: comprimentoLinha,
                height: expessuraLinha,
              ),
            ],
          ),
        ],
      )
    );
  }

  Widget _buildItemAvatar({
    @required BuildContext buildContext,
    ItemAvatar itemAvatar,
    FASES fase}) {

    double tamanhoImageIcone = (radiusItemAvatar + (radiusItemAvatar / 2));

    Image imagem;
    if (itemAvatar != null && itemAvatar.urlImagem != null) {
      imagem = Image(
        image: NetworkImage(itemAvatar.urlImagem),
        height: tamanhoImageIcone,
      );
    }

    List<Widget> widgets = List<Widget>();

    widgets.add(CircleAvatar(
      backgroundColor: corFundoAvatar,
      child: (imagem != null) ? imagem : Icon(Icons.security, color: Colors.black26, size: tamanhoImageIcone),
      radius: radiusItemAvatar,
    ));

    if (exibirLabel != null && exibirLabel && itemAvatar.label != null) {
      widgets.add(_getWidgetLabel(buildContext, itemAvatar.label));
    }

    if (exibirPontos && fase != FASES.FINAL && itemAvatar.pontos != null) {
      widgets.add(_getWidgetPontos(buildContext, itemAvatar.pontos));
    }

    if (exibirQualificados && (itemAvatar != null && itemAvatar.qualificado != null && !itemAvatar.qualificado) && fase != FASES.FINAL) {
      widgets.add(_getWidgetQualificado());
    }

    return Container(
      child: Stack(
        children: widgets,
      ),
    );
  }

  Widget _getWidgetQualificado() {
    return Container(
      width: radiusItemAvatar * 2,
      height: radiusItemAvatar * 2,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(2.0),
        color: Color.fromRGBO(189, 194, 199, 0.5),
      ),
    );
  }

  Widget _getWidgetPontos(BuildContext context, String pontos) {
    return Positioned(
      top: 0.0,
      right: 0.0,
      child: Container(
        padding: EdgeInsets.all(2.0),
        width: comprimentoLinha,
        decoration: BoxDecoration(
          color: Colors.red[800],
        ),
        child: FittedBox(
          fit: BoxFit.fitWidth,
          child: Text(
            pontos,
            style: TextStyle(
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Widget _getWidgetLabel(BuildContext context, String label) {
    return Positioned(
      bottom: 0.0,            
      child: Container(
        padding: EdgeInsets.only(top: 2.0, bottom: 2.0, left: 3.0, right: 3.0),
        width: radiusItemAvatar * 2,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: BorderRadius.circular(2.0)
        ),
        child: Align(
          alignment: Alignment.center,
          child: FittedBox(
            fit: BoxFit.fitWidth,
            child:  Text(
              label,
              style: TextStyle(
                color: Colors.white,
                fontSize: 12.0
              ),
            ),
          ),
        ),
      ),
    );
  }

  double _calcularAlturaLinhaConfronto(FASES fase) {
    double calculo = 0;
    double alturaTimeAvatar = radiusItemAvatar * 2;

    switch (fase) {
      case FASES.FASE1:
        calculo = alturaTimeAvatar + (expessuraLinha * 2) + alturaSizedBox;
        break;
      case FASES.FASE2:
        calculo = (alturaTimeAvatar * 2) + (expessuraLinha * 2) + (alturaSizedBox * 2);
        break;
      case FASES.FASE3:
        calculo = (alturaTimeAvatar * 4) + (expessuraLinha * 2) + (alturaSizedBox * 4);
        break;
      default: 

        calculo = _calcularMargeFaseFinal(alturaTimeAvatar);
        break;
    }
    
    return calculo;
  }

  double _calcularMargeFaseFinal(double alturaTimeAvatar) {
    double calculo = 0.0;
    if (!exibirFase1 && !exibirFase2) {
      calculo = (alturaTimeAvatar * 2) + (expessuraLinha * 2) + (alturaSizedBox * 2);
    } else {
      calculo = ((alturaTimeAvatar * 3) + (alturaTimeAvatar / 2)) + (expessuraLinha * 2) + (alturaSizedBox * 3);
    }
    return calculo;
  }

  double _calcularMargeFaseFase3() {
    double calculo = 0.0;
    if (exibirFase1 || exibirFase2) {
      calculo = ((radiusItemAvatar * 3) + (expessuraLinha * 2) + alturaSizedBox);
    }
    return calculo;
  }

}

class ItemAvatar {
  String label;
  String urlImagem;
  String pontos;
  bool qualificado = false;

  ItemAvatar(this.label, this.urlImagem, {this.qualificado, this.pontos});

  static ItemAvatar getItemPadrao() {
    return ItemAvatar(
      null,
      null,
    );
  }
}